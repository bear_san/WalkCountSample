# HealthKitを用いて歩数取得するサンプルアプリ
- このサンプルでは、2023年1月1日から実行当日までの歩数を1日ごとに取得し、UITableViewに表示します
- プログラムの見通しを良くするため、Swift Concurrencyを使用しています

## 動作画面
![IMG_3D9C0739FD9E-1](/uploads/cb244b54f4faeb664c8a08679343b626/IMG_3D9C0739FD9E-1.jpeg)
