//
//  ViewController.swift
//  WalkCountSample
//
//  Created by BearSan on 2023/02/01.
//

import UIKit
import HealthKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var results: [String] = []
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = results[indexPath.row]
        
        return cell
    }
    
    @IBAction func getWalkSteps(_ sender: Any) {
        results.removeAll()
        
        // 非同期処理が行われるため、Taskを使う
        Task{
            let store = HKHealthStore()
            
            //MARK: 【STEP1】歩数データへのアクセス許可を求める（プライバシー関係）
            let dataTypes = Set([HKObjectType.quantityType(forIdentifier: .stepCount)!])
            // アクセス許可を求めるデータの種類（今回は歩数のみ）
            
            do {
                try await store.requestAuthorization(toShare: Set(),
                                                     read: dataTypes)
                
                //MEMO: 順次実行のわかりやすさのためにawaitを使っていますが、↓こんな書き方↓もOK
                /*
                 HKHealthStore().requestAuthorization(toShare: nil,
                 read: dataTypes) { success, _ in
                 // アクセス許可を求めた結果が出たときの処理、successがtrueになっていればアクセスOK
                 
                 }
                 */
                
            } catch {
                // await使ったときは、try~catch節でエラー処理する
                fatalError("アクセス許可がされなかったか、エラーが発生しました")
            }
            
            //MARK: 【STEP2】取得したいデータの範囲を設定する
            let calendar = Calendar.current
            
            let start = DateComponents(calendar: .current,
                                       year: 2023,
                                       month: 1,
                                       day: 1)
            let startDate = calendar.date(from: start) // 歩数を取り出し始める日
            let predicate = HKQuery.predicateForSamples(withStart: startDate, end: Date()) // startから現在までの歩数を取得
            
            //MARK: 【STEP3】歩数の統計データを取得する
            guard let type = HKSampleType.quantityType(forIdentifier: .stepCount) else {
                return
            }
            guard let anchorDate = start.date else {
                return
            }
            
            let query = HKStatisticsCollectionQueryDescriptor(predicate: .quantitySample(type: type,
                                                                                        predicate: predicate),
                                                              options: .cumulativeSum,
                                                              anchorDate: anchorDate,
                                                              intervalComponents: DateComponents(day: 1))

            //MARK: 【STEP4】歩数の統計データから、歩数の実際の数を取り出す
            let stepCounts = try? await query.result(for: store)
            stepCounts?.enumerateStatistics(from: anchorDate, to: Date(), with: { statistics, _ in
                // statisticsはその日の歩数に関するデータ（歩数そのものや、開始時刻・終了時刻などが含まれる
                let stepCountInDay = statistics.sumQuantity()?.doubleValue(for: .count()) ?? 0 // なければ0歩
                
                // 日付整形用
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "YYYY年MM月dd日"
                
                let resultText = "\(dateFormatter.string(from: statistics.startDate))→\(stepCountInDay)歩"
                print(resultText)
                self.results.append(resultText)
            })
            
            tableView.reloadData()
        }
    }
}

